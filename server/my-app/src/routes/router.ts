import * as _ from 'lodash';
import { Router as ExpressRouter, Request, Response, NextFunction } from 'express';
import expressJwt from 'express-jwt';
import expressGraphql from 'express-graphql';
import { Auth } from './auth/auth';
import { Badger } from './auth/badger';

import badgerSchema from '../badger/graphql/badgerSchema';

export class Router {
  public static getRoutes() { return new Router().routes; }
  private routes = ExpressRouter();

  constructor() {
    this.routes.post('/authenticate', Auth.authenticate);
    this.routes.get('/badger', Badger.allBadgers);
    this.routes.get('/badger/:id', Badger.badgerById);
    this.routes.post('/badger', Badger.createBadger);
    this.routes.put('/badger/:id', Badger.updateBadger);
    this.routes.delete('/badger/:id', Badger.deleteBadger);

    this.routes.all('/graphql', expressJwt({ secret: process.env.JWT_SECRET }), this.getGraphql);
  }

  private nocache(req: Request, res: Response, next: NextFunction) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '0');
    res.header('Pragma', 'no-cache');
    next();
  }

  private getGraphql(req: Request, res: Response, next: NextFunction) {
    const jwt = Auth.getJWT(req);
    const graphql = expressGraphql({
      schema: getSchema(_.get(jwt, 'namespace')),
      pretty: true,
      graphiql: false,
    });
    return graphql(req, res);
  }
}

const getSchema = (namespace: string) => {
  let graphqlSchema;
  switch (_.lowerCase(namespace)) {
    default:
      graphqlSchema = badgerSchema;
      break;
  }
  return graphqlSchema;
};

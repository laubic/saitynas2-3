import { Request, Response, NextFunction } from 'express';
import {getJwt} from './auth';
import * as jwt from 'jsonwebtoken';
import * as conn from './db';
import connection from '../../badger/db/connection';
import  * as _ from 'lodash';

import logger from '../../logger';
import bodyParser = require('body-parser');

export class Badger {

  public static badgerById(req: Request, res: Response, next: NextFunction) {
    const id  = _.split(req.url, '/')[2];
    if (connection) {
      connection.models.badger.find({ where: { id } }).then((found : any) => {
        return res.status(200).send(found);
      }).catch((e :any) => {
        return res.status(500).send({ error: 'loadFailed' });
      });
    } else {
      return res.status(404).send();
    }

  }
  public static allBadgers(req: Request, res: Response, next: NextFunction) {
    if (connection) {
      connection.models.badger.findAll().then((found:any) => {
        return res.status(200).send(found);
      }).catch((e:any) => {
        return res.status(500).send({ error: 'loadFailed' });
      });
    } else {
      return res.status(404).send();

    }
  }
  public static createBadger(req: Request, res: Response, next: NextFunction) {
    const { type, locationFound, weight, age } = req.body;
    connection.models.badger.
    create({ type, weight, age, locationFound }).then((a:any) => {
      return res.status(201).send();
    }).catch((e :any) => {
      return res.status(500).send({ error: 'creationFailed' });
    });
    
  }
  public static updateBadger(req: Request, res: Response, next: NextFunction) {
    const { badger } = req.body;
    const { id } = badger;
    if (connection) {
      connection.models.badger.update(badger,{ where:{ id } }).then((found : any) => {
        return res.status(204).send();
      }).catch((e :any) => {
        return res.status(500).send({ error: 'updateFailed' });
      });
    } else {
      return res.status(404).send();
    }
  }
  public static deleteBadger(req: Request, res: Response, next: NextFunction) {
    const jwt = auth.
    const id = req.url.split('/')[2];
    if (connection) {
      connection.models.badger.destroy({ where: { id } }).then((found : any) => {
        return res.status(204).send({ message: 'success' });
      }).catch((e :any) => {
        return res.status(500).send({ error: 'deleteFailed' });
      });
    } else {
      return res.status(404).send();
    }
  }
}

import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import * as conn from './db';
import logger from '../../logger';

export class Auth {

  public static authenticate(req: Request, res: Response, next: NextFunction) {
    conn.db.tx((t: any) => {
      const username = req.body.username;
      return t.one(
        `select u.* from badger.users u ` +
        `where lower(u.username) = lower($1)`,
        username);
    }).then((result: any) => {
      if (result.password !== req.body.password) {
        return res.status(401).send('The username or password don\'t match');
      }
      const payload = {
        clientId: result.clientId,
        email: result.email,
        id: result.id,
        language: result.language,
        name: result.name,
        permissions: (result.role === 'ADMIN') ? [result.role, 'USER'] : [result.role],
        role: result.role,
        surname: result.surname,
        username: result.username,
        namespace: result.namespace,
      };
      const options = {
        expiresIn: 60 * 60 * 18,
      };
      res.status(201).send({ id_token: jwt.sign(payload, process.env.JWT_SECRET, options) });
    }).catch((err: Error) => {
      logger('Failed authenticating', err);
      return res.status(401).send('The username or password don\'t match');
    });
  }

  public static getJWT(req: Request) {
    const authHeader = req.header('authorization');
    if (authHeader && authHeader.split(' ')[0] === 'Bearer') {
      const token = authHeader.split(' ')[1];
      return jwt.decode(token);
    }
    return null;
  }
}

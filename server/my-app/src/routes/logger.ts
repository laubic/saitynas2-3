import * as express from 'express';
import logger from '../logger'

export class Logger {

  public static log(req: express.Request, res: express.Response) {
    logger(JSON.stringify(req.body));
    res.status(200).end();
  }

}

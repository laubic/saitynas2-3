import { Sequelize } from 'sequelize';
import logger from '../../logger';
import { user } from './user';
import { badger } from './badger';

const defineBadgerModel = (conn: Sequelize, badgerSchema: string, sync?: boolean) => {
  const userModel = conn.define('user', user, { schema: badgerSchema });
  const badgerModel = conn.define('badger', badger,{ schema: badgerSchema });
  if (sync) return conn.sync({ force: false, alter: false })
    .then(() => logger('DB schema %s synced with sequelize' + badgerSchema));
};

export { defineBadgerModel };

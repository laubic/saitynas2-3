import graphqlDate from 'graphql-date';
import { makeExecutableSchema } from 'graphql-tools';
import { merge } from 'lodash';
import * as log4js from 'log4js';
import { userType, userResolvers, userQuery, userMutation } from './user';


const root = `
    scalar Date
    type Query {
        ${userQuery}
    }
    type Mutation {
        ${userMutation}
    }
    schema {
        query: Query
        mutation: Mutation
    }
`;

const typeDefs = [
  userType,
  root,
];

const rootResolvers = {};

const resolvers: {} = merge(
  { Date: graphqlDate },
  rootResolvers,
  userResolvers,
);

const logger = { log: (e: Error) => log4js.getLogger('api').error(e.stack) };

export default makeExecutableSchema({
  typeDefs,
  resolvers,
  logger,
});

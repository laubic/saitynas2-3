import * as bodyParser from 'body-parser';
import * as log4js from 'log4js';
import { Application, NextFunction, Request, Response } from 'express';
import { Router } from './routes/router';

const dotenv: any = require('dotenv');
const cors: any = require('cors');
const errorhandler: any = require('errorhandler');

export class Server {
  public static getApp(): Application {
    return new Server().app;
  }
  private app: Application;

  constructor() {
    this.app = require('express')();
    this.config();
    this.routes();
  }

  private config() {
    dotenv.load({});

    this.app.use(bodyParser.json({ limit: '10mb' }));
    this.app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));
    this.app.use(cors({}));

    const logger = log4js.getLogger('api');
    const msg = ':remote-addr [:date] \':method :url\' :status :res[content-length] :response-time';
    this.app.use(log4js.connectLogger(logger, { level: 'auto', format: msg }));

    if (process.env.NODE_ENV === 'development') {
      this.app.use(errorhandler({}));
    }

    // catch 404 and forward to error handler
    this.app.use((err: any, req: Request, res: Response, next: NextFunction) => {
      err.status = 404;
      logger.warn(JSON.stringify(err));
      next(err);
    });

    this.app.use((err: any, req: Request, res: Response, next: NextFunction) => {
      if (err.name === 'StatusError') {
        res.status(err.status).send(err.message);
      } else {
        logger.warn(JSON.stringify(err));
        next(err);
      }
    });
  }

  private routes() {
    this.app.use(Router.getRoutes());
  }
}

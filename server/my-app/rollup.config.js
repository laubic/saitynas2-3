// rollup.config.js

import typescript from 'rollup-plugin-typescript';
import uglify from 'rollup-plugin-uglify';
import progress from 'rollup-plugin-progress';
import filesize from 'rollup-plugin-filesize';
import sourcemaps from 'rollup-plugin-sourcemaps';
import async from 'rollup-plugin-async';
import {
  minify
} from 'uglify-es'

export default {
  entry: 'src/server.ts',
  format: 'cjs',
  plugins: [
    async(),
    typescript({
      typescript: require('typescript')
    }),
    uglify({}, minify),
    sourcemaps(),
    progress(),
    filesize(),
  ],
  sourceMap: true,
  external: [
    'bluebird',
    'body-parser',
    'cors',
    'debug',
    'dotenv',
    'errorhandler',
    'express',
    'express-graphql',
    'express-jwt',
    'express-jwt-permissions',
    'graphiql',
    'graphql',
    'graphql-date',
    'graphql-tools',
    'handlebars',
    'html-loader',
    'json-loader',
    'jsonwebtoken',
    'lodash',
    'log4js',
    'moment',
    'moment-timezone',
    'mongoose',
    'path',
    'pg',
    'pg-promise',
    'request',
    'request-promise',
    'sequelize',
    'source-map-support'
  ],
  watch: {
    usePolling: true,
    interval: 100,
  },
  dest: 'build/backend.js'
};
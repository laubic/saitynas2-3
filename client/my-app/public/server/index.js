const express = require('express');
const proxy = require('http-proxy-middleware');
const path = require('path');
const app = express();
const PORT = process.env.PORT || 3000;

const log4js = require('log4js');
log4js.configure({
    appenders: [
        {
            category: 'app',
            layout: {
                pattern: '[%d{ISO8601}] [%[%5.5p%]] %m',
                type: 'pattern',
            },
            type: 'console',
        },
        {
            category: 'app',
            host: process.env.LOG_HOST || 'log',
            port: process.env.LOG_PORT || 5000,
            type: 'logstashUDP',
        }
    ]
});
const logger = log4js.getLogger('app');
logger.info('Logger have been configured for module: app');
app.use(log4js.connectLogger(logger, { level: 'auto' }));

app.use(express.static(path.resolve(__dirname, '..', 'build')));

app.use('/api', proxy({target: process.env.POSTGRES_API, changeOrigin: true}));
app.use('/authenticate', proxy({target: process.env.POSTGRES_API, changeOrigin: true}));
app.use('/logger', proxy({target: process.env.POSTGRES_API, changeOrigin: true}));

app.get('/*', function(req, res) {
    res.sendFile(path.resolve(__dirname, '..', 'build', 'index.html'));
});


app.listen(PORT, function() {
    logger.info('Express server listening on port ' + PORT)
});

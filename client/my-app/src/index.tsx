import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as moment from 'moment';
import * as injectTapEventPlugin from 'react-tap-event-plugin';
import * as StackTrace from 'stacktrace-js';
import * as log4javascript from 'log4javascript';
import { ApolloProvider } from 'react-apollo';
import { Route, Switch } from 'react-router';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import createBrowserHistory from 'history/createBrowserHistory';
import IntlWrapper from './languages/IntlWrapper';
import ReactApollo from './ReactApollo';
import { configureStore } from './configureStore';
import ThemeProvider from './themes/ThemeProvider';
import MainPage from './modules/main/containers/MainPage';
import LoginPage from './modules/login/containers/LoginPage';
import 'moment/locale/lt';
import 'es6-shim';
import './index.css';

injectTapEventPlugin(); // Needed for onTouchTap for Material UI
log4javascript.logLog.setQuietMode(true);
const ajaxAppender = new log4javascript.AjaxAppender('/logger');
ajaxAppender.setThreshold(log4javascript.Level.INFO);
window.onerror = (message: string, file: string, line: number, col: number, error: Error) => {
  StackTrace.fromError(error).then((stackframes: StackTrace.StackFrame[]) => {

    }).catch((err: Error) => {
      // tslint:disable-next-line
      console.log(err.message);
    });
};

moment.locale('lt');

const history = createBrowserHistory();
const middleware = routerMiddleware(history);
const apollo = ReactApollo.getApolloClient();
const store = configureStore(middleware);

ReactDOM.render(
  <ThemeProvider>
    <ApolloProvider client={apollo} store={store}>
      <IntlWrapper>
        <ConnectedRouter history={history}>
          <Switch>
            <Route exact path="/login" component={LoginPage} />
            <Route path="/" component={MainPage} />
          </Switch>
        </ConnectedRouter>
      </IntlWrapper>
    </ApolloProvider>
  </ThemeProvider>,
  document.getElementById('app-container') as HTMLElement,
);

import 'intl';
import { addLocaleData } from 'react-intl';

// Adding intl polifill for browser which do not support it yet
const areIntlLocalesSupported = require('intl-locales-supported');
const localesMyAppSupports = ['lt'];
// ['ee', 'en', 'es', 'lt', 'lv', 'pl', 'ru'];
if (global.Intl) {
  // Determine if the built-in `Intl` has the locale data we need.
  if (!areIntlLocalesSupported(localesMyAppSupports)) {
    // `Intl` exists, but it doesn't have the data we need, so load the
    // polyfill and patch the constructors we need with the polyfill's.
    const intlPolyfill = require('intl');
    Intl.NumberFormat = intlPolyfill.NumberFormat;
    Intl.DateTimeFormat = intlPolyfill.DateTimeFormat;
  }
} else {
  // No `Intl`, so use and load the polyfill.
  global.Intl = require('intl');
}

import * as enLocale from 'react-intl/locale-data/en';
import * as ltLocale from 'react-intl/locale-data/lt';

import en from './en';
import lt from './lt';

import 'intl/locale-data/jsonp/en';
import 'intl/locale-data/jsonp/lt';

export default function loadMessages(userLanguage: string) {
  switch (userLanguage) {

    case 'EN':
      addLocaleData([...enLocale]);
      return en;
    case 'LT':
    default:
      addLocaleData([...ltLocale]);
      return lt;
  }
}

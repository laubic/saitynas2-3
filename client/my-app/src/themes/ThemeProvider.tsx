import * as React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import lightTheme from './lightTheme';

export interface ThemeProviderProps {
}

const themeProvider: React.StatelessComponent<ThemeProviderProps> = (props) => {
  const { children } = props;
  return (
    <MuiThemeProvider muiTheme={getMuiTheme(lightTheme)}>
      {children}
    </MuiThemeProvider>
  );
};

export default themeProvider;

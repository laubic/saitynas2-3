import * as React from 'react';

export interface RowProps {
    className?: string;
}

export const Row: React.StatelessComponent<RowProps> = props => {
    const { className, children } = props;

    return (
        <div className="row">
            <div className="col-12">
                <div className={`form-group ${className ? className : ''}`}>
                    {children}
                </div>
            </div>
        </div>
    );
};

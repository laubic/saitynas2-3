import * as React from 'react';
import * as moment from 'moment';
import { TouchTapEventHandler } from 'material-ui';
import IconButton from 'material-ui/IconButton';
import { default as ArrowLeft } from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import { default as ArrowRight } from 'material-ui/svg-icons/hardware/keyboard-arrow-right';
import DatePicker from './DatePicker';

export interface WeekPickerProps {
  value: moment.Moment;
  onChange: (date: moment.Moment) => void;
  decrementWeek: TouchTapEventHandler;
  incrementWeek: TouchTapEventHandler;
}

const weekPicker: React.StatelessComponent<WeekPickerProps> = (props) => {
  const { value, decrementWeek, incrementWeek, onChange } = props;
  const iconColor = 'rgb(0, 151, 167)';
  const fieldStyle = { width: '174px' };

  const formatDate = (date: Date) => {
    return moment(date).weekday(0).startOf('day').format('YYYY.MM.DD') + ' - '
      + moment(date).weekday(0).startOf('day').add(7 - 1, 'days').format('YYYY.MM.DD');
  };

  return (
    <div className="btn-group">
      <IconButton onTouchTap={decrementWeek} >
        <ArrowLeft color={iconColor} />
      </IconButton>
      <IconButton onTouchTap={incrementWeek} >
        <ArrowRight color={iconColor} />
      </IconButton>
      <DatePicker
        onChange={onChange}
        date={value}
        formatDate={formatDate}
        textFieldStyle={fieldStyle} />
    </div >
  );
};

export default weekPicker;

import * as React from 'react';
import { touch } from 'redux-form';
import { SelectField as ReduxFormMaterialUISelectField } from 'redux-form-material-ui';

// tslint:disable-next-line
const selectField: React.StatelessComponent<any> = props => {
  const { input, meta, children } = props;
  const componentProps = {
    dropDownMenuProps: {
      onClose: () => meta.dispatch(touch(meta.form, input.name)),
      ...props.dropDownMenuprops,
    },
    ...props,
  };
  return (
    <ReduxFormMaterialUISelectField  {...componentProps} >
      {children}
    </ReduxFormMaterialUISelectField>
  );
};

export default selectField;

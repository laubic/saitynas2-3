import * as React from 'react';
import CircularProgress from 'material-ui/CircularProgress';

export interface LoaderProps {
  size?: number;
}

const loader: React.StatelessComponent<LoaderProps> = (props) => {
  const { size } = props;
  return (
    <CircularProgress size={size} />
  );
};

export default loader;

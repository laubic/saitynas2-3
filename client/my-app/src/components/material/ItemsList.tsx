import * as React from 'react';
import * as _ from 'lodash';
import ConfirmModalDialog from '../form/ConfirmModalDialog';
import { injectIntl, InjectedIntlProps } from 'react-intl';
import ListItemWithMenu from './ListItemWithMenu';
import { List } from 'material-ui/List';
import Loader from '../material/Loader';

interface ItemsListProps<T> {
  onDelete: (item: T) => void;
  onEdit: (item: T) => void;
  onClick?: (item: T) => void;
  items: T[];
  primaryTextProperty: string | string[];
  secondaryTextProperty?: string | string[];
  secondaryTextName?: string | string[];
  deleteMessage: string;
  loading: boolean;
  showAmmount?: number;
  copyButton?: boolean;
}

interface ItemsListState<T> {
  showModal: boolean;
  item?: T;
}

class ItemsList<T> extends
  React.Component<ItemsListProps<T> & InjectedIntlProps, ItemsListState<T>> {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
    };
    this.close = this.close.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.getPrimaryText = this.getPrimaryText.bind(this);
  }
  public static defaultProps = { showAmmount: 100 };

  close() {
    this.setState({
      showModal: false,
      item: undefined,
    });
  }

  open(event: Event, item: T) {
    event.preventDefault();
    this.setState({
      item,
      showModal: true,
    });
  }

  onEdit(event: Event, item: T) {
    event.preventDefault();
    this.props.onEdit(item);
  }

  onDelete() {
    if (this.state.item) {
      this.props.onDelete(this.state.item);
    }
    this.close();
  }
  getPrimaryText(item: {}) {
    const primaryTextProperty = this.props.primaryTextProperty;
    if (_.isArray(primaryTextProperty)) {
      let result = '';
      primaryTextProperty.forEach((property) => {
        result += item[property] + ' ';
      });
      return result;
    }
    return item[primaryTextProperty];
  }
  getSecondaryText(item: {}) {
    const { intl, secondaryTextProperty, secondaryTextName } = this.props;
    if (_.isArray(secondaryTextProperty)) {
      let result = '';
      secondaryTextProperty.forEach((property, index) => {
        if (secondaryTextName && secondaryTextName[index])
          result += intl.formatMessage({ id: secondaryTextName[index] }) + ': ';
        if (property && item[property] !== null)
          result += item[property];
        result += '         ';
      });
      return result;
    }
    if (secondaryTextProperty && secondaryTextName && !_.isArray(secondaryTextName)) {
      return intl.formatMessage({ id: secondaryTextName })
        + ': ' + item[secondaryTextProperty];
    }
    return '';

  }

  render() {
    const { items, onClick, deleteMessage, loading } = this.props;

    return (
      <List>
        {loading && <Loader />}
        <ConfirmModalDialog
          show={this.state.showModal}
          onClose={this.close}
          onConfirm={this.onDelete}
          cancelMessageKey="cancel"
          confirmMessageKey="delete"
          dialogMessageKey={deleteMessage}
        />

        {items && items.slice(0, this.props.showAmmount).map((item: T, index: number) => {
          return (
            <ListItemWithMenu
              key={index}
              primaryText={this.getPrimaryText(item)}
              secondaryText={this.getSecondaryText(item)}
              onTouchTap={() => onClick ? onClick(item) : undefined}
              onEditClick={(event => this.onEdit(event, item))}
              onRemoveClick={(event => this.open(event, item))}
              copyButton={this.props.copyButton}
            />
          );
        })}
      </List >
    );
  }

}

export default injectIntl(ItemsList);

import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import IconButton from 'material-ui/IconButton';
import { default as NavigationClose } from 'material-ui/svg-icons/navigation/close';

export interface ModalDialogTitleProps {
  title: string;
  close: () => void;
}

const modalDialogTitle: React.StatelessComponent<ModalDialogTitleProps> = (props) => {
  const { close, title } = props;
  return (
    <div>
      <FormattedMessage id={title} />
      <IconButton
        className="float-right"
        style={{ marginTop: -17, marginRight: -10 }}
        onTouchTap={close}
      >
        <NavigationClose />
      </IconButton>
      <span className="space" />
    </div>
  );
};

export default modalDialogTitle;

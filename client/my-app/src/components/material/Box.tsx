import * as React from 'react';

export interface BoxProps {
  header?: React.ReactNode;
  body: React.ReactNode;
  className?: string;
  bodyClassName?: string;
  bodyStyle?: React.CSSProperties;
  divider?: boolean;
}

const box: React.StatelessComponent<BoxProps> = (props) => {
  const { header, body, className, bodyStyle, bodyClassName, divider } = props;

  return (
    <div className={`box ${className ? className : ''}`}>
      {header && <div className="box-header">{header}</div>}
      {header && divider && <div className="box-divider" />}
      <div className={`box-body ${bodyClassName ? bodyClassName : ''}`}
        style={bodyStyle}>{body}</div>
    </div>
  );
};

box.defaultProps = {
  className: 'box-default',
  divider: true,
};

export default box;

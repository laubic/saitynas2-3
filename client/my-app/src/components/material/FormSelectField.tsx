import * as React from 'react';
import MenuItem from 'material-ui/MenuItem';
import { Validator } from 'redux-form';
import SelectField from '../validation/SelectField';
import FormField from '../../components/form/FormField';

interface SelectFieldProps {
  value?: string;
  label: string;
  options: {}[];
  valueField: string;
  textField: string;
  fullWidth?: boolean;
  name: string;
  onFieldSelect?: (value: any) => void;
  validate?: Validator | Validator[];
  colClassName?: string;
  style?: React.CSSProperties;
}

const selectField: React.StatelessComponent<SelectFieldProps> = (props) => {
  const { label, options, valueField, textField, fullWidth, name,
    validate, colClassName, onFieldSelect } = props;
  return (
    <FormField
      name={name}
      label={label}
      component={SelectField}
      fullWidth={fullWidth}
      validate={validate}
      colClassName={colClassName || 'col-sm-12'}
      style={props.style}
    >
      {options && options.map((option, index) => {
        return <MenuItem
          onTouchTap={onFieldSelect}
          key={index} value={option[valueField]}
          primaryText={option[textField]}
        />;
      })}
    </FormField>
  );

};

export default selectField;

import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Field, BaseFieldProps } from 'redux-form';
import { TextField } from 'redux-form-material-ui';

interface FormFieldProps {
  name: string;
  component?: React.ReactNode;
  hint?: string;
  type?: string;
  fullWidth?: boolean;
  label?: string;
  colClassName?: string;
  disabled?: boolean;
  underlineDisabledStyle?: {};
  style?: React.CSSProperties;
}

const formField: React.StatelessComponent<BaseFieldProps & FormFieldProps> = (props) => {
  const { label, children, colClassName, hint, ...rest } = props;
  const floatingLabelText = label ? <FormattedMessage id={label} /> : undefined;
  const hintText = hint ? <FormattedMessage id={hint} /> : undefined;
  return (
    <div className={colClassName}>
      <Field {...rest} floatingLabelText={floatingLabelText} hintText={hintText} >
        {children}
      </Field>
    </div>
  );
};

formField.defaultProps = {
  component: TextField,
  fullWidth: true,
  type: 'text',
  colClassName: 'col-sm-12',
  disabled: false,
};

export default formField;

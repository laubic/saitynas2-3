import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Field, BaseFieldProps } from 'redux-form';
import { AutoComplete as ReduxFormAutoComplete } from 'redux-form-material-ui';
import AutoComplete from 'material-ui/AutoComplete';

interface FormAutoCompleteFieldProps {
  name: string;
  hint?: string;
  fullWidth?: boolean;
  label?: string;
  colClassName?: string;
  dataSource: string[];
  maxSearchResults?: number;
}

type ComponentProps = BaseFieldProps & FormAutoCompleteFieldProps;

const formAutoCompleteField: React.StatelessComponent<ComponentProps> = (props) => {
  const { label, dataSource, colClassName, hint, name, ...rest } = props;
  const floatingLabelText = label ? <FormattedMessage id={label} /> : undefined;
  const hintText = hint ? <FormattedMessage id={hint} /> : undefined;
  return (
    <div className={colClassName}>
      <Field
        component={ReduxFormAutoComplete}
        openOnFocus={true}
        name={name}
        id={name}
        filter={AutoComplete.caseInsensitiveFilter}
        hintText={hintText}
        floatingLabelText={floatingLabelText}
        dataSource={dataSource}
        {...rest}
      />
    </div>
  );
};

formAutoCompleteField.defaultProps = {
  fullWidth: true,
  colClassName: 'col-sm-12',
  maxSearchResults: 10,
};

export default formAutoCompleteField;

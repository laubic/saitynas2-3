import * as React from 'react';
import { default as ContentSave } from 'material-ui/svg-icons/content/save';
import { default as NavigationCancel } from 'material-ui/svg-icons/navigation/cancel';
import FormTitleWithButtons from './FormTitleWithButtons';
import FloatingButtonWithToolTip from '../material/FloatingButtonWithToolTip';

interface BasicFormTitleProps {
  onSave: () => void;
  onCancel: () => void;
  title: string;
  cancelTooltip?: string;
  saveTooltip?: string;
}

const basicFormTitle: React.StatelessComponent<BasicFormTitleProps> = (props) => {
  const { onCancel, onSave, title, saveTooltip, cancelTooltip } = props;
  return (
    <FormTitleWithButtons title={title}>
      <FloatingButtonWithToolTip
        tooltipId={saveTooltip}
        secondary={true}
        onMouseDown={onSave}>
        <ContentSave />
      </FloatingButtonWithToolTip>
      <span className="space" />
      <FloatingButtonWithToolTip
        tooltipId={cancelTooltip}
        onMouseDown={onCancel}>
        <NavigationCancel />
      </FloatingButtonWithToolTip>
    </FormTitleWithButtons>
  );

};
basicFormTitle.defaultProps = {
  cancelTooltip: 'button.cancel.tooltip',
  saveTooltip: 'button.save.tooltip',
};

export default basicFormTitle;

import * as React from 'react';
import { FormattedMessage } from 'react-intl';

interface FormTitleProps {
  title: string;
  formatTitle?: boolean;
}

const formTitle: React.StatelessComponent<FormTitleProps> = (props) => {
  const { title, children, formatTitle } = props;
  return (
    <div>
      {formatTitle ? <FormattedMessage id={title} /> : title}
      {children}
    </div>
  );
};

formTitle.defaultProps = {
  formatTitle: true,
};

export default formTitle;

import { ApolloClient, createNetworkInterface } from 'react-apollo';

import Auth from './Auth';

const networkInterface = createNetworkInterface({
  uri: '/graphql',
});

networkInterface.use([{
  // tslint:disable-next-line
  applyMiddleware(req: any, next: Function) {
    if (!req.options.headers) {
      req.options.headers = {};
    }
    req.options.headers.authorization = `Bearer ${Auth.getToken()}`;
    next();
  },
}]);

class Apollo {
  private static instance: ApolloClient = new ApolloClient({
    networkInterface,
    connectToDevTools: true,
  });

  static getApolloClient() {
    return Apollo.instance;
  }
}

export default Apollo;

import * as React from 'react';
import Article from '../../../components/material/Article';

export interface HeaderBodyLayoutProps {
  body?: React.ReactNode;
  header?: React.ReactNode;
}

const headerBodyLayout: React.StatelessComponent<HeaderBodyLayoutProps> = (props) => {
  const { body, header } = props;

  return (
    <div className="row">
      <Article headerTitle={header} className="full-width">
        <div className="row">
          <div className="col-xs-12 col-lg-12">
            {body}
          </div>
        </div>
      </Article>
    </div>
  );
};

export default headerBodyLayout;

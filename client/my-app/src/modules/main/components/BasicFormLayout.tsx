import * as React from 'react';
import FormLayout from './FormLayout';
import Box from '../../../components/material/Box';

export interface BasicFormLayoutProps {
  body?: React.ReactNode;
  title?: React.ReactNode;
  bodyHeader?: React.ReactNode;
}

const basicFormLayout: React.StatelessComponent<BasicFormLayoutProps> = (props) => {
  const { body, title, bodyHeader } = props;

  return (
    <FormLayout title={title}>
      <Box body={body} header={bodyHeader} />
    </FormLayout>
  );
};

export default basicFormLayout;

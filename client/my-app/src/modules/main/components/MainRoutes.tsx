import * as React from 'react';
import * as _ from 'lodash';
import BadgerRoutes from './BadgerRoutes';
import Auth from '../../../Auth';

interface MainRoutesProps {
  matchUrl: string;
}

const mainRoutes: React.StatelessComponent<MainRoutesProps> = (props) => {
  const { matchUrl } = props;
  const namespace = Auth.getJWT().namespace;

  let routes;
  switch (_.lowerCase(namespace)) {
    default:
      routes = <BadgerRoutes matchUrl={matchUrl} />;
      break;
  }

  return (
    <div>
      {routes}
    </div>
  );
};

export default mainRoutes;

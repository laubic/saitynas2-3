import * as React from 'react';
import { Route, Switch } from 'react-router';
import UsersPanel from '../../user/containers/UsersPanel';
import UserCreatePanel from '../../user/containers/UserCreatePanel';
import UserEditPanel from '../../user/containers/UserEditPanel';
import UserViewPanel from '../../user/containers/UserViewPanel';
import UserPasswordPanel from '../../user/containers/UserPasswordPanel';
import BadgerPanel from '../../badger/containers/BadgerPanel';
import BadgerCreatePanel from '../../badger/containers/BadgerCreatePanel';
import BadgerEditPanel from '../../badger/containers/BadgerEditPanel';

interface FactoryRoutesProps {
  matchUrl: string;
}

const factoryRoutes: React.StatelessComponent<FactoryRoutesProps> = (props) => {
  const { matchUrl } = props;
  return (
    <Switch>
      <Route path={matchUrl + 'settings/users/:id/edit'} component={UserEditPanel} />
      <Route path={matchUrl + 'settings/users/:id/changePassword'} component={UserPasswordPanel} />
      <Route path={matchUrl + 'settings/users/new'} component={UserCreatePanel} />
      <Route path={matchUrl + 'settings/users/:id'} component={UserViewPanel} />
      <Route path={matchUrl + 'settings/users'} component={UsersPanel} />
      <Route path={matchUrl + 'badgers/:id/edit'} component={BadgerEditPanel} />
      <Route path={matchUrl + 'badgers'} component={BadgerPanel} />
      <Route path={matchUrl + 'badger/new'} component={BadgerCreatePanel} />

    </Switch>
  );
};

export default factoryRoutes;

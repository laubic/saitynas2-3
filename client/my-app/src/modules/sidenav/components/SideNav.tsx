import * as React from 'react';
import { Link } from 'react-router-dom';
import Icon from '../../../components/material/Icon';

export interface SideNavProps {
  about: string;
  brand: JSX.Element;
  onBrand: () => void;
  navCollapsed: boolean;
  toggleCollapsedNav: (collapsed: boolean) => void;
}

const sideNav: React.StatelessComponent<SideNavProps> = (props) => {
  const { brand, navCollapsed, onBrand, toggleCollapsedNav } = props;

  const onToggleCollapsedNav = () => {
    const collapsed = !navCollapsed;
    toggleCollapsedNav(collapsed);
  };

  return (
    <nav className="app-sidebar bg-color-dark">
      <section className="sidebar-header bg-color-dark">
        <img src="/favicon.ico" style={{ width: 19, height: 19, margin: '10px 10px 18px' }} />
        <Link to="/" onClick={onBrand} className="brand">{brand}</Link>
        <a href="javascript:;" className="collapsednav-toggler" onClick={onToggleCollapsedNav}>
          <Icon name={navCollapsed ? 'radio_button_unchecked' : 'radio_button_checked'} />
        </a>
      </section>

      <section className="sidebar-content">
        {props.children}
      </section>
    </nav>
  );
};

export default sideNav;

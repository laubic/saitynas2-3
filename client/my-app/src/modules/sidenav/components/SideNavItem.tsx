import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { TouchTapEventHandler } from 'material-ui';
import { Link } from 'react-router-dom';
import Icon from '../../../components/material/Icon';
import FlatButton from 'material-ui/FlatButton';

export interface SideNavItemProps {
  link?: string;
  icon?: string;
  label: string;
  onTouchTap?: TouchTapEventHandler;
}
const sideNavItem: React.StatelessComponent<SideNavItemProps> = (props) => {
  const { link, icon, label, onTouchTap, children } = props;
  const linkElement = link ? <Link to={link} /> : <a />;
  return (
    <li>
      <FlatButton containerElement={linkElement} onTouchTap={onTouchTap}>
        {icon && <Icon name={icon} className="nav-icon" />}
        <span className="nav-text"><FormattedMessage id={label} /></span>
      </FlatButton>
      {children && <ul>{children}</ul>}
    </li>
  );
};

sideNavItem.defaultProps = {
  icon: 'keyboard_arrow_right',
};

export default sideNavItem;

import * as React from 'react';
import SideNavItem from './SideNavItem';
import SideNavContent from './SideNavContent';
import Auth from '../../../Auth';

export interface FactorySideNavProps {
  path: string;
}

const factorySideNav: React.StatelessComponent<FactorySideNavProps> = (props) => {
  const { path } = props;
  const isAdmin = Auth.isAdmin();
  return (
    <SideNavContent path={path}>
          <SideNavItem link="/badgers" label="badgers" />
      {isAdmin && <li className="nav-divider" />}
      {isAdmin &&
        <SideNavItem label="administration" link="/settings" icon="settings">
          <SideNavItem link="/settings/users" label="users" />
        </SideNavItem>
      }
    </SideNavContent>
  );
};

export default factorySideNav;

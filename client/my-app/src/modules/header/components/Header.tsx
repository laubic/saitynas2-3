import * as React from 'react';
import { Link } from 'react-router-dom';
import Icon from '../../../components/material/Icon';

export interface HeaderProps {
  brand: JSX.Element;
  onBrand: () => void;
}

export class Header extends React.Component<HeaderProps, {}> {
  private sidebarBtn: HTMLAnchorElement;

  componentDidMount() {
    const sidebarToggler = this.sidebarBtn;
    const $sidebarToggler = $(sidebarToggler);
    const $body = $('#body');
    $sidebarToggler.on('click', (e) => {
      $body.toggleClass('sidebar-mobile-open');
    });
  }

  render() {
    return (
      <section className="app-header">
        <div className="app-header-inner bg-color-dark">
          <div className="hidden-lg-up float-left">
            <a
              href="javascript:;"
              className="md-button header-icon toggle-sidebar-btn"
              ref={(c: HTMLAnchorElement) => { this.sidebarBtn = c; }}
            >
              <Icon name="menu" />
            </a>
          </div>
          <div className="brand hidden-md-down">
            <h2>
              <img src="/favicon.ico" style={{ width: 23, height: 23, margin: '10px 10px 14px' }} />
              <Link to="/" onClick={this.props.onBrand}>
                {this.props.brand}
              </Link>
            </h2>
          </div>
          <div className="top-nav-right">
            {this.props.children}
          </div>
        </div>
      </section>
    );
  }
}

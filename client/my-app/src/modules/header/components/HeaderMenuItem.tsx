import * as React from 'react';
import { Link } from 'react-router-dom';
import { MenuItem, TouchTapEventHandler } from 'material-ui';
import { FormattedMessage } from 'react-intl';

export interface HeaderMenuItemProps {
  iconName: string;
  onTouchTap?: TouchTapEventHandler;
  message: string;
  link?: string;
  value?: string;
  className?: string;
  muiName?: string;
}

export class HeaderMenuItem extends React.Component<HeaderMenuItemProps, {}> {
  static muiName = 'MenuItem';

  render() {
    const { iconName, onTouchTap, message, link } = this.props;
    const listItemStyle = { paddingLeft: '50px' };
    const menuItemStyle = { fontSize: '14px', lineHeight: '48px' };
    const linkElement = link ? <Link to={link} /> : undefined;
    return (
      <MenuItem
        containerElement={linkElement}
        primaryText={<FormattedMessage id={message} />}
        style={menuItemStyle}
        innerDivStyle={listItemStyle}
        leftIcon={<i className="material-icons">{iconName}</i>}
        onTouchTap={onTouchTap}

      />
    );
  }
}

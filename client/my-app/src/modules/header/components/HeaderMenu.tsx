import * as React from 'react';
import { IconMenu, FlatButton } from 'material-ui';

export interface HeaderMenuProps {
  label: React.ReactNode;
}

const headerMenu: React.StatelessComponent<HeaderMenuProps> = (props) => {
  const buttonStyle = { width: 'auto', height: '60px', color: '#fff' };

  return (
    <ul className="list-unstyled float-right">
      <li style={{ marginRight: '10px' }}>
        <IconMenu
          iconButtonElement={<FlatButton style={buttonStyle} label={props.label} />}
          anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
          targetOrigin={{ horizontal: 'right', vertical: 'top' }}
          menuStyle={{ minWidth: '150px' }}
        >
          {props.children}
        </IconMenu>
      </li>
    </ul>
  );
};

export default headerMenu;

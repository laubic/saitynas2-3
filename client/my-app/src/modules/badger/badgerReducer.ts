import { push } from 'react-router-redux';
import { Action } from 'redux';
import { BadgerApi,BadgerCreateResponse,
   BadgerDeleteResponse, BadgerLoadResponse, 
   BadgerUpdateResponse, BadgersLoadResponse } from './badgerApi';
import { Badger } from './Model';
import { displaySnackbar } from '../main/snackbarReducer';

// Actions
const LOAD_BADGERS =      'LOAD_BADGERS';
const LOAD_BADGERS_SUCC = 'LOAD_BADGERS_SUCC';
const LOAD_BADGERS_FAIL = 'LOAD_BADGERS_FAIL';
const LOAD_BADGER =       'LOAD_BADGER';
const LOAD_BADGER_SUCC =  'LOAD_BADGER_SUCC';
const LOAD_BADGER_FAIL =  'LOAD_BADGER_FAIL';
const UPDATE_BADGER =         'UPDATE_BADGER';
const UPDATE_BADGER_SUCC =    'UPDATE_BADGER_SUCC';
const UPDATE_BADGER_FAIL =    'UPDATE_BADGER_FAIL';
const DELETE_BADGER =         'DELETE_BADGER';
const DELETE_BADGER_SUCC =    'DELETE_BADGER_SUCC';
const DELETE_BADGER_FAIL =    'DELETE_BADGER_FAIL';
const CREATE_BADGER =         'DELETE_BADGER';
const CREATE_BADGER_SUCC =    'DELETE_BADGER_SUCC';
const CREATE_BADGER_FAIL =    'DELETE_BADGER_FAIL';

// Reducer
interface BadgerAction extends Action {
  badger?: Badger;
  badgers?: Badger[];
  errorMessage?: string;
}

export interface BadgerReducerState {
  badger?: Badger;
  badgers: Badger[];
  loadingBadgers: boolean;
  loadingBadger: boolean;
  error?: boolean;
  errorMessage?: string;

}

const initialState: BadgerReducerState = {
  loadingBadger: true,
  loadingBadgers:true,
  badgers: [],  
};

export const badgerReducer =
  (state: BadgerReducerState = initialState, action: BadgerAction): BadgerReducerState => {
    switch (action.type) {
      case LOAD_BADGERS:
        return {
          ...state,
          loadingBadgers: false,
        };
      case LOAD_BADGERS_SUCC:
        return {
          ...state,
          loadingBadgers: false,
          error: false,
          badgers: action.badgers!,
        };
      case LOAD_BADGERS_FAIL:
        return {
          ...state,
          loadingBadgers: false,
          error: true,
          errorMessage: action.errorMessage!,
        };
      case LOAD_BADGER:
        return {
          ...state,
          loadingBadger: false,
        };
      case LOAD_BADGER_SUCC:
        return {
          ...state,
          loadingBadger: false,
          error: false,
          badger: action.badger!,
        };
      case LOAD_BADGER_FAIL:
        return {
          ...state,
          loadingBadger: false,
          error: true,
          errorMessage: action.errorMessage!,
        };
      case UPDATE_BADGER:
        return {
          ...state,
          loadingBadger: false,
        };
      case UPDATE_BADGER_SUCC:
        return {
          ...state,
          error: false,
          badgers: action.badgers!,
        };
      case UPDATE_BADGER_FAIL:
        return {
          ...state,
          error: true,
          errorMessage: action.errorMessage!,
        };
      case DELETE_BADGER:
        return {
          ...state,
        };
      case DELETE_BADGER_SUCC:
        return {
          ...state,
          error: false,
          badgers: action.badgers!,
        };
      case DELETE_BADGER_FAIL:
        return {
          ...state,
          error: true,
          errorMessage: action.errorMessage!,
        };
      case CREATE_BADGER:
        return {
          ...state,
        };
      case CREATE_BADGER_SUCC:
        return {
          ...state,
          error: false,
          badgers: action.badgers!,
        };
      case CREATE_BADGER_FAIL:
        return {
          ...state,
          error: true,
          errorMessage: action.errorMessage!,
        };
      default:
        return state;
    }
  };

// Action Creators
function startLoadingBadgers() {
  return { type: LOAD_BADGERS };
}

function receiveBadgers(badgers: BadgersLoadResponse) {
  return {
    badgers,
    type: LOAD_BADGERS_SUCC,
  };
}

function badgersError(message: string) {
  return {
    message,
    type: LOAD_BADGERS_FAIL,
  };
}
export interface LoadBadgers {
  (): (dispatch: Function) => void;
}

export const loadBadgers: LoadBadgers = () => {
  return async (dispatch: Function) => {
    dispatch(startLoadingBadgers());
    return BadgerApi.loadBadgers().then((response: Response) => {
      if (response.ok) {
        response.json().then((json: BadgersLoadResponse) => {
          dispatch(receiveBadgers(json));
        });
      } else {
        dispatch(badgersError(response.statusText));
        dispatch(displaySnackbar('badgers.load.error', 'danger'));
      }
    }).catch((error: Error) => {
      throw error;
    });
  };
};

function startLoadingBader() {
  return { type: LOAD_BADGER };
}

function receiveBadger(badger: BadgerLoadResponse) {
  return {
    badger,
    type: LOAD_BADGER_SUCC,
  };
}

function badgerError(message: string) {
  return {
    message,
    type: LOAD_BADGER_FAIL,
  };
}
export interface LoadBadger {
  (id:number): (dispatch: Function) => void;
}

export const loadBadger: LoadBadger = (id) => {
  return async (dispatch: Function) => {
    dispatch(startLoadingBader());
    return BadgerApi.loadBadger(id).then((response: Response) => {
      if (response.ok) {
        response.json().then((json: BadgerLoadResponse) => {
          dispatch(receiveBadger(json));
          
        });
      } else {
        dispatch(badgerError(response.statusText));
      }
    }).catch((error: Error) => {
      throw error;
    });
  };
};

function startUpdate() {
  return { type: UPDATE_BADGER };
}

function updateSucc() {
  return {
    type: UPDATE_BADGER_SUCC,
  };
}

function updateFail(message: string) {
  return {
    message,
    type: UPDATE_BADGER_FAIL,
  };
}
export interface UpdateBadger {
  (badger:Badger): (dispatch: Function) => void;
}

export const updateBadger: UpdateBadger = (badger) => {
  return async (dispatch: Function) => {
    dispatch(startUpdate());
    return BadgerApi.updateBadger(badger).then((response: Response) => {
      if (response.ok) {
        response.json().then((json: BadgerUpdateResponse) => {
          dispatch(updateSucc());
          dispatch(displaySnackbar('badger.update.success','success'));
          dispatch(push('badgers'));
        });
      } else {
        dispatch(updateFail(response.statusText));
      }
    }).catch((error: Error) => {
      throw error;
    });
  };
};

function startDelete() {
  return { type: DELETE_BADGER };
}

function deleteSucc() {
  return {
    type: DELETE_BADGER_SUCC,
  };
}

function deleteFail(message: string) {
  return {
    message,
    type: DELETE_BADGER_FAIL,
  };
}
export interface DeleteBadger {
  (id:number): (dispatch: Function) => void;
}

export const deleteBadger: DeleteBadger = (id) => {
  return async (dispatch: Function) => {
    dispatch(startDelete());
    return BadgerApi.deleteBadger(id).then((response: Response) => {
      if (response.ok) {
        response.json().then((json: BadgerDeleteResponse) => {
          dispatch(deleteSucc());
          dispatch(push('badgers'));
        });
      } else {
        dispatch(deleteFail(response.statusText));
      }
    }).catch((error: Error) => {
      throw error;
    });
  };
};

function startCreate() {
  return { type: CREATE_BADGER };
}

function createSucc() {
  return {
    type: CREATE_BADGER_SUCC,
  };
}

function createFail(message: string) {
  return {
    message,
    type: CREATE_BADGER_FAIL,
  };
}
export interface CreateBadger {
  (badger:Badger): (dispatch: Function) => void;
}

export const createBadger: CreateBadger = (badger:Badger) => {
  return async (dispatch: Function) => {
    dispatch(startCreate());
    return BadgerApi.createBadger(badger).then((response: Response) => {
      if (response.ok) {
        response.json().then((json: BadgerCreateResponse) => {
          dispatch(createSucc());
          dispatch(displaySnackbar('badger.create.success','success'));
          dispatch(push('/badgers'));
        });
      } else {
        dispatch(createFail(response.statusText));
        dispatch(displaySnackbar('badger.create.failed', 'danger'));
      }
    }).catch((error: Error) => {
      throw error;
    });
  };
};

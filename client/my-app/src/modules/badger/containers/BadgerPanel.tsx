import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { push } from 'react-router-redux';
import { connect, Dispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loadBadgers,LoadBadgers,deleteBadger ,DeleteBadger} from '../badgerReducer';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import { Badger } from '../Model';
import FilterFormLayout from '../../main/components/FilterFormLayout';
import ItemsList from '../../../components/material/ItemsList';
import ItemsListTitle from '../../../components/material/ItemsListTitle';

interface StateProps {
  badgers: Badger[];
  loading: boolean;
  error?: boolean;
  errorMessage?: string;
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  loadBadgers:LoadBadgers;
  deleteBadger:DeleteBadger;

}

export interface BadgerPanelProps extends RouteComponentProps<{}> { }

type ComponentProps = StateProps & DispatchProps & BadgerPanelProps;

class BadgerPanel extends React.Component<ComponentProps, {}> {

  constructor() {
    super();
    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
  }
  componentWillMount() {
    this.props.loadBadgers();
  }
  onDelete(badger: Badger) {
    this.props.deleteBadger(badger.id!);
    this.props.loadBadgers();
  }

  onEdit(badger: Badger) {
    this.props.push(`/badgers/${badger.id}/edit`);
  }

  render() {
    const { push, badgers, loading } = this.props;
    return (
      <FilterFormLayout
        title={
          <ItemsListTitle
            onNewClick={() => push('/badger/new')}
            title="badgers.title"
            createTooltip="badgers.create.tooltip"
          />}
        body={(
          <ItemsList
            items={badgers}
            onDelete={this.onDelete}
            onEdit={this.onEdit}
            deleteMessage="badger.delete.confirm"
            primaryTextProperty={['id', 'type']}
            loading={loading}
          />
        )}
      />
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps =>
 bindActionCreators({ loadBadgers,deleteBadger, push,displaySnackbar }, dispatch);

const mapStateToProps = (state: State, ownProps: BadgerPanelProps): StateProps => {
  return {
    badgers: state.badgerReducer.badgers,
    loading: state.badgerReducer.loadingBadgers,
    error: state.badgerReducer.error,
    errorMessage: state.badgerReducer.errorMessage,
  };
};

const badgerPanel: React.ComponentClass<BadgerPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(BadgerPanel);

export default badgerPanel;

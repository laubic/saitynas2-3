import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { RouteComponentProps } from 'react-router';
import { loadBadger,LoadBadger,updateBadger,UpdateBadger } from '../badgerReducer';

import { submit } from 'redux-form';
import { Badger } from '../Model';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import * as _ from 'lodash';
import BadgerForm from '../components/BadgerForm';
import BasicFormLayout from '../../main/components/BasicFormLayout';
import BasicFormTitle from '../../../components/form/BasicFormTitle';

interface StateProps {
  badger?:Badger;
  loading: boolean;
  error?: boolean;
  errorMessage?: string;
}

interface DispatchProps {
  push: typeof push;
  updateBadger:UpdateBadger;
  loadBadger: LoadBadger;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
}

export interface BadgerEditPanelProps extends RouteComponentProps<{ id: string }> {
}

type ComponentProps = StateProps & DispatchProps  & BadgerEditPanelProps;

class BadgerEditPanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
  }
  componentWillMount() {
    this.props.loadBadger(_.parseInt(this.props.match.params.id));
  }
  render() {
    const { updateBadger, badger,loading } = this.props;
    return (
      <BasicFormLayout
        title={
          <BasicFormTitle
            onSave={() => this.props.submit('BadgerForm')}
            onCancel={() => this.props.push('/badgers')}
            title="badger.modify.title"
          />}
        body={
          !loading ?
          <BadgerForm
            onSubmit={updateBadger}
            initialValues={badger!}
          /> : <div/>}
      />
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps =>
 bindActionCreators({ loadBadger,updateBadger, push,displaySnackbar, submit }, dispatch);

const mapStateToProps = (state: State, ownProps: BadgerEditPanelProps): StateProps => {
  return {
    badger: state.badgerReducer.badger,
    loading: state.badgerReducer.loadingBadger,
    error: state.badgerReducer.error,
    errorMessage: state.badgerReducer.errorMessage,
  };
};

const badgerPanel: React.ComponentClass<BadgerEditPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(BadgerEditPanel);

export default badgerPanel;

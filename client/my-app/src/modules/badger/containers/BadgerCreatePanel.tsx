import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { RouteComponentProps } from 'react-router';
import { submit } from 'redux-form';
import { Badger } from '../Model';
import { createBadger,CreateBadger } from '../badgerReducer';

import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import BadgerForm from '../components/BadgerForm';
import BasicFormLayout from '../../main/components/BasicFormLayout';
import BasicFormTitle from '../../../components/form/BasicFormTitle';

interface StateProps {
  badger?:Badger;
  error?: boolean;
  errorMessage?: string;
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
  createBadger:CreateBadger;

}

export interface BadgerCreatePanelProps extends RouteComponentProps<{}> {
}

type ComponentProps = StateProps & DispatchProps & BadgerCreatePanelProps;

class BadgerCreatePanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <BasicFormLayout
        title={
          <BasicFormTitle
            onSave={() => this.props.submit('BadgerForm')}
            onCancel={() => this.props.push('/badgers')}
            title="badger.create.title"
          />}
        body={<BadgerForm onSubmit={this.props.createBadger} />}
      />
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps =>
 bindActionCreators({ createBadger, push,displaySnackbar, submit }, dispatch);

const mapStateToProps = (state: State, ownProps: BadgerCreatePanelProps): StateProps => {
  return {
    badger: state.badgerReducer.badger,
    error: state.badgerReducer.error,
    errorMessage: state.badgerReducer.errorMessage,
  };
};

const badgerCreatePanel: React.ComponentClass<BadgerCreatePanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(BadgerCreatePanel);

export default badgerCreatePanel;

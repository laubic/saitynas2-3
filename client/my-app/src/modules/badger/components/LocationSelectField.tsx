import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Validator } from 'redux-form';
import FormSelectField from '../../../components/material/FormSelectField';

export interface LocationSelectFieldProps {
  name: string;
  validate?: Validator | Validator[];
  value?: string;
}

const locationSelectField: React.StatelessComponent<LocationSelectFieldProps> = (props) => {
  const { name, validate, value } = props;
  return (
    <FormSelectField
      value={value}
      options={[
        { value: 'SF', label: <FormattedMessage id="badger.location.South" /> },
        { value: 'NF', label: <FormattedMessage id="badger.location.North" /> },
        { value: 'WF', label: <FormattedMessage id="badger.location.West" /> },
        { value: 'EF', label: <FormattedMessage id="badger.location.East" /> },
      ]}
      textField="label"
      valueField="value"
      label="badger.location"
      fullWidth={true}
      name={name}
      validate={validate}
    />
  );
};

export default locationSelectField;

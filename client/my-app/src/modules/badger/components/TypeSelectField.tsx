import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Validator } from 'redux-form';
import FormSelectField from '../../../components/material/FormSelectField';

export interface TypeSelectFieldProps {
  name: string;
  validate?: Validator | Validator[];
  value?: string;
  style?: React.CSSProperties;
}

const typeSelectField: React.StatelessComponent<TypeSelectFieldProps> = (props) => {
  const { name, validate, value } = props;
  return (
    <FormSelectField
      value={value}
      options={[
        { value: 'Grey', label: <FormattedMessage id="badger.type.Grey" /> },
        { value: 'EN', label: <FormattedMessage id="badger.type.Black" /> },
        { value: 'ES', label: <FormattedMessage id="badger.type.Brown" /> },
      ]}
      textField="label"
      valueField="value"
      label="badger.type.select"
      fullWidth={true}
      name={name}
      validate={validate}
      style={props.style}
    />
  );
};

export default typeSelectField;

import * as React from 'react';
import { reduxForm, FormProps } from 'redux-form';
import { State } from '../../../rootReducer';
import { required } from '../../../components/validation/ValidationUtils';
import { Badger } from '../Model';
import FormField from '../../../components/form/FormField';
import LocationSelectField from './LocationSelectField';
import TypeSelectField from './TypeSelectField';

export interface BadgerFormProps {
  onSubmit: (badger: Badger) => void;
  hidePassword?: boolean;
}

export interface BadgerFormModel {
  weight: string;
  age: string;
  locationFound: string;
  type: string;
  id: number;
}

type ComponentProps = BadgerFormProps & FormProps<BadgerFormModel, BadgerFormProps, State>;

class BadgerForm extends React.Component<ComponentProps, {}> {
  constructor(props) {
    super(props);
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <form onSubmit={handleSubmit}>
        <div className="row">
          <div className="col-md-6">
              <FormField name="weight"
              type={'number'}label="badger.weight" validate={required} />
            </div>
            <div className="col-md-6">
              <FormField type= {'number'}name="age" label="badger.age" validate={required} />
            </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <TypeSelectField name="type" validate= {required} />
          </div>
          <div className="col-md-6">
            <LocationSelectField name="locationFound" validate={required} />
          </div>
        </div>
      </form>
    );
  }
}

export default reduxForm<BadgerFormModel, BadgerFormProps, State>({
  form: 'BadgerForm',
})(BadgerForm);

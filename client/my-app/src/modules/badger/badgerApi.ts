import 'whatwg-fetch';
import { Badger } from './Model';
import Auth from '../../Auth';
export interface BadgersLoadResponse {
  badgers: Badger[];
}
export interface BadgerLoadResponse {
  badger: Badger;
}
export interface BadgerCreateResponse {
  created: boolean;
}
export interface BadgerUpdateResponse {
  updated: boolean;
}
export interface BadgerDeleteResponse {
  deleted: boolean;
}

export class BadgerApi {
  static loadBadgers() {
    const options: RequestInit = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    };
    return fetch('/badger', options);
  }
  static loadBadger(id:number) {
    const options: RequestInit = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    };
    return fetch('/badger/' + id, options);
  }
  static updateBadger(badger:Badger) {
    const options: RequestInit = {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body:JSON.stringify({ badger }),
    };
    return fetch('/badger/' + badger.id, options);
  }
  static deleteBadger(id:number) {
    const options: RequestInit = {
      method: 'DELETE',
      headers: 
      { 'Content-Type': 'application/json',
        authentication: Auth.getToken()!      },
    };
    return fetch('/badger/' + id, options);
  }
  static createBadger(badger:Badger) {
    const options: RequestInit = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body:JSON.stringify(badger),
    };
    return fetch('/badger', options);
  }
}

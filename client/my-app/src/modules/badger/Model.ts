export interface Badger {
  id?: number;
  locationFound: string;
  type?: string;
  weight?: BadgerType;
  age?: string;
}

export type BadgerType = 'Pilkasis' | 'Juodasis' | 'Rudasis';


import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { graphql, ApolloQueryResult } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { submit } from 'redux-form';
import { User, Password } from '../Model';
import { changePassword, usersQuery } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../../modules/main/snackbarReducer';
import Auth from '../../../Auth';
import UserPasswordForm from '../components/UserPasswordForm';
import BasicFormLayout from '../../../modules/main/components/BasicFormLayout';
import BasicFormTitle from '../../../components/form/BasicFormTitle';

interface StateProps {
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
}

interface GraphqlProps {
  refetch: Function;
  changePassword: (userId: number, oldPassword: string, newPassword: string) =>
    Promise<ApolloQueryResult<{ changePassword: { success: boolean } }>>;
  user: User;
  users: User[];
}

export interface UserEditPanelProps extends RouteComponentProps<{ id: string }> {
}

type ComponentProps = StateProps & DispatchProps & GraphqlProps & UserEditPanelProps;

class UserEditPanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
    this.changePassword = this.changePassword.bind(this);
    this.getPath = this.getPath.bind(this);
  }

  changePassword(password: Password) {
    this.props.changePassword(this.props.user.id!, password.oldPassword, password.newPassword1)
      .then((result) => {
        const success = result.data.changePassword.success;
        if (success) this.props.displaySnackbar('user.password.changed', 'success');
        else this.props.displaySnackbar('user.password.change.failed', 'danger');
        this.props.push(this.getPath());
      });
  }

  getPath() {
    const prevPath = _.get(this.props.location.state, 'prevPath');
    return prevPath ? prevPath : `/settings/users/${this.props.user.id}`;
  }

  render() {
    const { push, submit, user } = this.props;
    return (
      <BasicFormLayout
        title={
          <BasicFormTitle
            onSave={() => submit('UserPasswordForm')}
            onCancel={() => push(this.getPath())}
            title="user.password.change.title"
          />}
        body={
          <UserPasswordForm
            onSubmit={this.changePassword}
            oldPasswordRequired={!Auth.isAdmin()} />
        }
        bodyHeader={
          _.get(user, 'name') + ' ' + _.get(user, 'surname')
        }
      />
    );
  }
}

const findUser = (id: number, users: User[]) => {
  if (id && users && users.length > 0) {
    return Object.assign({}, users.find((user: User) => user.id === id));
  } else {
    return undefined;
  }
};

const mapStateToProps = (state: State, ownProps: UserEditPanelProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
    submit: bindActionCreators(submit, dispatch),
  };
};

const withData = graphql<{ users: User[] }, ComponentProps>(usersQuery, {
  props: (props) => {
    const { data, ownProps } = props;
    const { refetch, loading, users } = data!;
    const { id } = ownProps.match.params;
    return {
      refetch,
      loading,
      users,
      user: findUser(parseInt(id, 10), users),
    };
  },
});

const withMutations = graphql<{}, ComponentProps>(changePassword, {
  props: (props) => {
    const { mutate } = props;
    return {
      changePassword: (userId, oldPassword, newPassword) => {
        return mutate!({ variables: { userId, oldPassword, newPassword } });
      },
    };
  },
});

const userEditPanel: React.ComponentClass<UserEditPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withData(withMutations(UserEditPanel)));

export default userEditPanel;

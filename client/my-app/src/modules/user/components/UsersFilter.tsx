import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { reduxForm, Field, FormProps } from 'redux-form';
import { TextField } from 'redux-form-material-ui';
import { State } from '../../../rootReducer';
import Box from '../../../components/material/Box';

export interface UsersFilterProps {
}

export interface UsersFilterModel {
  userName: string;
}

class UsersFilter extends
  React.Component<UsersFilterProps & FormProps<UsersFilterModel, UsersFilterProps, State>, {}> {

  render() {
    return (
      <Box
        body={(
          <div className="form-group">
            <Field
              component={TextField}
              hintText={<FormattedMessage id="user.filter" />}
              name="userFullname"
              fullWidth={true}
            />
          </div>
        )} />
    );
  }
}

export default reduxForm<UsersFilterModel, UsersFilterProps, State>({
  form: 'UsersFilter',
  onSubmit: undefined,
})(UsersFilter);

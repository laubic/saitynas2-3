import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { User } from '../Model';

export interface UserViewProps {
  user: User;
}

const userView: React.StatelessComponent<UserViewProps> = (props) => {
  const { user } = props;
  return (
    <div>
      {user &&
        <div>
          <div className="row">
            <div className="col-3"><FormattedMessage id="user.name" />:</div>
            <div className="col-6">{user.name}</div>
          </div>
          <div className="row">
            <div className="col-3"><FormattedMessage id="user.surname" />:</div>
            <div className="col-6">{user.surname}</div>
          </div>
          <div className="row">
            <div className="col-3"><FormattedMessage id="user.role" />:</div>
            <div className="col-6"><FormattedMessage id={'user.role.' + user.role} /></div>
          </div>
          <div className="row">
            <div className="col-3"><FormattedMessage id="user.email" />:</div>
            <div className="col-6">{user.email}</div>
          </div>
          <div className="row">
            <div className="col-3"><FormattedMessage id="user.username" />:</div>
            <div className="col-6">{user.username}</div>
          </div>
        </div>
      }
    </div>
  );
};

export default userView;

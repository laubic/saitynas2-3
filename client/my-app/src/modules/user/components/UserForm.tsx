import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { reduxForm, FormProps } from 'redux-form';
import { State } from '../../../rootReducer';
import { required } from '../../../components/validation/ValidationUtils';
import { User } from '../Model';
import FormField from '../../../components/form/FormField';
import RoleSelectField from './RoleSelectField';
import LanguageSelectField from './LanguageSelectField';

export interface UserFormProps {
  onSubmit: (user: User) => void;
  users: User[];
  hidePassword?: boolean;
}

export interface UserFormModel {
  name: string;
  surname: string;
  role: string;
  email: string;
  language: string;
  username: string;
  password: string;
}

type ComponentProps = UserFormProps & FormProps<UserFormModel, UserFormProps, State>;

class UserForm extends React.Component<ComponentProps, {}> {
  constructor(props) {
    super(props);
    this.validateUsername = this.validateUsername.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
  }

  validateUsername(value: string, allValues: Object[]) {
    const users = this.props.users;
    if (users) {
      const usernameExists = this.props.users.find((user: User) => {
        return (user.username === value) && (user.id !== allValues['id']);
      });
      if (usernameExists) return <FormattedMessage id="user.username.unique" />;
    }
    return undefined;
  }

  validateEmail(value: string, allValues: Object[]) {
    const users = this.props.users;
    if (users) {
      const emailExists = this.props.users.find((user: User) => {
        return (user.email === value) && (user.id !== allValues['id']);
      });
      if (emailExists) return <FormattedMessage id="user.email.unique" />;
    }
    return undefined;
  }

  // TODO enable LanguageSelectField when needed
  render() {
    const { handleSubmit, hidePassword } = this.props;
    return (
      <form onSubmit={handleSubmit}>
        <FormField name="name" label="user.name" validate={required} />
        <FormField name="surname" label="user.surname" validate={required} />
        <RoleSelectField name="role" validate={required} />
        <FormField name="email" label="user.email" validate={required} />
        <LanguageSelectField name="language" style={{ display: 'none' }} />
        <FormField
          name="username"
          label="user.username"
          validate={[required, this.validateUsername]}
        />
        {!hidePassword && <FormField name="password" label="user.password" validate={required} />}
      </form>
    );
  }
}

export default reduxForm<UserFormModel, UserFormProps, State>({
  form: 'UserForm',
})(UserForm);

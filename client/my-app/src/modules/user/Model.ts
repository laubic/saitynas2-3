export interface User {
  id?: number;
  name: string;
  surname?: string;
  role?: Role;
  email?: string;
  username?: string;
  password?: string;
  language?: Language;
  todayTaskCount?: number;
  hasTaskUpdates?: boolean;
  tasksStatus?: number;
  createdAt?: string;
  updatedAt?: string;
}

export type Role = 'USER' | 'ADMIN';

export type Language = 'EE' | 'EN' | 'ES' | 'LT' | 'LV' | 'PL' | 'RU';

export interface Password {
  oldPassword: string;
  newPassword1: string;
  newPassword2: string;
}

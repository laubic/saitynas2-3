import { combineReducers, Reducer, Action } from 'redux';
import { routerReducer, RouterState } from 'react-router-redux';
import { reducer as formReducer, FormStateMap } from 'redux-form';
import { sessionReducer, SessionReducerState } from './modules/login/sessionReducer';
import { badgerReducer, BadgerReducerState } from './modules/badger/badgerReducer';
import { sideNavReducer, SideNavReducerState } from './modules/sidenav/sideNavReducer';
import { snackbarReducer, SnackbarReducerState } from './modules/main/snackbarReducer';
import ReactApollo from './ReactApollo';

export type State = {
  sessionReducer: SessionReducerState;
  sideNavReducer: SideNavReducerState;
  snackbarReducer: SnackbarReducerState;
  badgerReducer: BadgerReducerState;
  form: FormStateMap;
};

// Actions
export const STATE_RESET = 'STATE_RESET';

export interface BasicAction extends Action {
  type: string;
}

const appReducer = combineReducers<State>({
  badgerReducer,
  sessionReducer,
  sideNavReducer,
  snackbarReducer: snackbarReducer as Reducer<SnackbarReducerState>,
  form: formReducer,
  apollo: (ReactApollo.getApolloClient().reducer() as Reducer<{}>),
  router: routerReducer as Reducer<RouterState>,
});

const rootReducer = (state: State, action: BasicAction): State => {
  if (action.type === STATE_RESET) {
    // tslint:disable-next-line
    state = {} as State;
  }
  return appReducer(state, action);
};

// Action Creators
export interface ResetState {
  (): BasicAction;
}

export const resetState: ResetState = () => {
  return { type: STATE_RESET };
};

export default rootReducer;

#!/bin/bash

docker network create badger-network
docker volume create badger-volume

docker exec -i -t badger-postgres psql -U badger badger -f ./import.sql
